package in.silentsudo.apache.pulsar.samples;

import org.apache.pulsar.client.api.*;

public class MainReader {
    public static void main(String[] args) throws PulsarClientException, InterruptedException {
        System.out.println("Hello world!");
        PulsarClient pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();

        String topic = "persistent://manning/ashish/pulsar-learning";
        spawnConsumer(pulsarClient, topic);
    }


    private static void spawnConsumer(PulsarClient pulsarClient, String topic) throws PulsarClientException, InterruptedException {
        Reader<byte[]> reader = pulsarClient.newReader()
                .topic(topic)
                .startMessageId(MessageId.earliest)
                .readerName("pulsar-learning-reader-subscription")
                .create();
        while (true) {
            Message<byte[]> message = reader.readNext();
            if(message != null) {
//                System.out.println("Message key: " + new String(message.getKeyBytes()));
                System.out.println("Message data: " + new String(message.getValue()));
                System.out.println("Message properties: " + message.getProperties());
            }
        }
    }
}