package in.silentsudo.apache.pulsar.samples;

import org.apache.pulsar.client.api.*;

import java.util.UUID;

public class MainSynchronous {
    public static void main(String[] args) throws PulsarClientException, InterruptedException {
        System.out.println("Hello world!");
        PulsarClient pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();

        String topic = "persistent://manning/ashish/pulsar-learning";
//        Thread.sleep(1000);
//        spawnProducer(pulsarClient, topic);
        spawnConsumer(pulsarClient, topic);
    }

    private static void spawnProducer(PulsarClient pulsarClient, String topic) throws PulsarClientException, InterruptedException {

        Producer<byte[]> producer = pulsarClient.newProducer()
                .topic(topic)
                .create();

        for (int i = 0; i < 10; i++) {
            producer
                    .newMessage()
                    .key("message-" + i)
                    .value(("Hello Pulsar From Java API: " + i).getBytes())
                    .property("headerId", UUID.randomUUID().toString())
                    .property("userRequestId", UUID.randomUUID().toString())
                    .send();
            System.out.println("Produces message: " + i);
            Thread.sleep(1000);
        }
    }

    private static void spawnConsumer(PulsarClient pulsarClient, String topic) throws PulsarClientException, InterruptedException {
        Consumer<byte[]> subscribe = pulsarClient.newConsumer()
                .topic(topic)
                .subscriptionName("pulsar-learning-java-subscription")
                .subscribe();
        while (subscribe.isConnected()) {
            Thread.sleep(3);
            Message<byte[]> message = subscribe.receive();
            if (message != null) {
                subscribe.acknowledge(message);
                System.out.println("Message key: " + new String(message.getKeyBytes()));
                System.out.println("Message data: " + new String(message.getValue()));
                System.out.println("Message properties: " + message.getProperties());
            }
        }

    }
}