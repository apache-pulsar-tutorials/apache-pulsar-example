package in.silentsudo.apache.pulsar.samples;

import org.apache.pulsar.client.api.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

public class MainAsynchronous {
    public static void main(String[] args) throws PulsarClientException, InterruptedException {
        System.out.println("Hello world!");
        PulsarClient pulsarClient = PulsarClient.builder()
                .serviceUrl("pulsar://localhost:6650")
                .build();

        String topic = "persistent://manning/ashish/pulsar-learning";
        spawnProducer(pulsarClient, topic);
        spawnConsumer(pulsarClient, topic);
    }

    private static void spawnProducer(PulsarClient pulsarClient, String topic) throws PulsarClientException, InterruptedException {

        Producer<byte[]> producer = pulsarClient.newProducer()
                .topic(topic)
                .create();

        for (int i = 0; i < 1000; i++) {
            producer
                    .newMessage()
                    .key("message-" + i)
                    .value(("Hello Pulsar From Java API: " + i).getBytes())
                    .property("headerId", UUID.randomUUID().toString())
                    .property("userRequestId", UUID.randomUUID().toString())
                    .send();
            System.out.println("Produces message: " + i);
        }
    }

    private static void spawnConsumer(PulsarClient pulsarClient, String topic) throws PulsarClientException, InterruptedException {
        ConsumerBuilder<byte[]> consumerBuilder = pulsarClient.newConsumer()
                .topic(topic)
                .subscriptionName("pulsar-learning-java-subscription-shared")
                .subscriptionType(SubscriptionType.Shared)
                .messageListener((MessageListener<byte[]>) (consumer, message) -> {

                    if (message != null) {
                        try {
                            System.out.println("Message key: " + new String(message.getKeyBytes()));
                            System.out.println("Message data: " + new String(message.getValue()));
                            System.out.println("Message properties: " + message.getProperties());
                            System.out.println("Message from consumer: " + consumer.getConsumerName());
                            consumer.acknowledge(message);
                        } catch (PulsarClientException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });

        IntStream.range(0, 5)
                .forEach(i -> {
                    try {
                        consumerBuilder.consumerName("[consumer: " + i +"]").subscribe();
                    } catch (PulsarClientException e) {
                        System.out.println("Error subscribing consumer");
                        e.printStackTrace();
                    }
                });

    }
}