## Commands

```shell
 1949  ./pulsar-admin clusters list
 1950  ./pulsar-admin topics list
 1951  ./pulsar-admin topic list
 1952  ./pulsar-admin tenants list
 1953  ./pulsar-admin tenants create manning
 1954  ./pulsar-admin tenants list
 1955  ./pulsar-admin namespaces list
 1956  ./pulsar-admin namespaces list pulic
 1957  ./pulsar-admin namespaces list public
 1958  ./pulsar-admin namespaces list pulsar
 1959  ./pulsar-admin tenants list
 1960  ./pulsar-admin namespaces create manning/ashish
 1961  ./pulsar-admin namespaces list manning
 1962  ./pulsar-admin topics list
 1963  ./pulsar-admin topics list manning
 1964  ./pulsar-admin topics list manning/ashish
 1965  ./pulsar-admin namespaces list
 1966  ./pulsar-admin namespaces list manning
 1967  ./pulsar-admin topics create
 1968  clear
 1969  ./pulsar-admin namespaces developer
 1970  ./pulsar-admin namespaces create developer
 1971  ./pulsar-admin namespaces list manning
 1972  ./pulsar-admin topics create
 1973  ./pulsar-admin topics create persistent://manning/ashish/pulsar-learning
 1974  ./pulsar-admin topics list
 1975  ./pulsar-admin topics list manning/ashish
 1976  ./pulsar-admin topics list ashish
 1977  ./pulsar-admin topics list manning
 1978  ./pulsar-admin tenants list
 1979  ./pulsar-admin namespaces
 1980  ./pulsar-admin namespaces list
 1981  ./pulsar-admin namespaces list manning
 1982  ./pulsar-admin topics list manning/ashish
 1983  ./pulsar-admin topics create persistent://manning/ashish/pulsar-learning
 1984  ./pulsar-admin topics list manning/ashish
 1985  ./pulsar-client produce persistent://manning/ashish/pulsar-learning --num-produce 2 --messages "Hello Pulsar Learning"
 1986  ./pulsar-admin topics stats
 1987  ./pulsar-admin topics stats persistent://manning/ashish/pulsar-learning
 1988  ./pulsar-admin  persistent://manning/ashish/pulsar-learning peek-messages --count 10
 1989  ./pulsar-admin topics persistent://manning/ashish/pulsar-learning peek-messages --count 10 --subscription-name "top-10-peeks" as;
 1990  ./pulsar-admin topics peek-messages --count 10 --subscription-name "top-10-peeks" persistent://manning/ashish/pulsar-learning
 1991  ./pulsar-admin peek-messages --count 10 --subscription-name "top-10-peeks" persistent://manning/ashish/pulsar-learning
 1992  ./pulsar-admin Topic peek-messages --count 10 --subscription top-10-peeks persistent://manning/ashish/pulsar-learning
 1993  ./pulsar-admin topics peek-messages --count 10 --subscription top-10-peeks persistent://manning/ashish/pulsar-learning
 1994  ./pulsar-client --help
 1995  ./pulsar-admin topics list manning/ashish
 1996  ./pulsar-client consume
 1997  ./pulsar-admin topics list manning/ashish
 1998  ./pulsar-client consume persistent://manning/ashish/pulsar-learning
 1999  ./pulsar-client consume persistent://manning/ashish/pulsar-learning --num-messages 0--subscription-name pulsar-learning-subscription--subscription-type Exclusive
 2000  ./pulsar-client consume persistent://manning/ashish/pulsar-learning --num-messages 0 --subscription-name pulsar-learning-subscription --subscription-type Exclusive

```